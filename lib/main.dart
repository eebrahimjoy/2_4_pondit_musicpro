import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MusicPro(),
    ),
  );
}

void playSound(int? soundNumber) {
  final player = AudioCache(prefix: "assets/audio/");
  player.play('assets_note$soundNumber.wav');
}

Expanded buildMusicButton({Color? color, int? soundNumber}) {
  return Expanded(
    child: Container(
      color: color,
      child: TextButton(
        child: Text(""),
        onPressed: () {
          playSound(soundNumber);
        },
      ),
    ),
  );
}

class MusicPro extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          buildMusicButton(color: Colors.green,soundNumber: 1),
          buildMusicButton(color: Colors.orange,soundNumber: 2),
          buildMusicButton(color: Colors.blue,soundNumber: 3),
          buildMusicButton(color: Colors.yellow,soundNumber: 4),
          buildMusicButton(color: Colors.teal,soundNumber: 5),
          buildMusicButton(color: Colors.cyan,soundNumber: 6),
          buildMusicButton(color: Colors.red,soundNumber: 7),
        ],
      ),
    );
  }
}
